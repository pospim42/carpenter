package cz.cvut.fit.Java.bricks;

import cz.cvut.fit.Java.bricks.Brick;
import cz.cvut.fit.Java.bricks.ColorType;
import cz.cvut.fit.Java.bricks.Sets.Castle;
import cz.cvut.fit.Java.bricks.Sets.CompleteSet;

public  class Tester
{
    private double[] dimensions;
    public Tester(){}
    public void printBrick(Brick brick)
    {
        double a = brick.getType().getSize();
        dimensions = (brick.getClass().getSimpleName().equals("ROOF")) ? new double[]{a, a/2, a} :
                new double[]{a, a, a};
        System.out.printf("%s(%.1fx%.1fx%.1f)\n",
                brick.toString(), dimensions[0], dimensions[1], dimensions[2]);
    }
    public void printBrickCZ(Brick brick)
    {
        double a = brick.getType().getSize();
        dimensions = (brick.getClass().getSimpleName().equals("ROOF")) ? new double[]{a, a/2, a} :
                new double[]{a, a, a};
        System.out.printf("%s (%.1fx%.1fx%.1f)\n",
                brick.toStringCZ(), dimensions[0], dimensions[1], dimensions[2]);

    }
    public void testSet()
    {
        CompleteSet set = new CompleteSet("Complete set", Type.CHILD);
        set.packBricks(ColorType.BLUE);
        set.printCZ();
        Castle castle = new Castle("Castle", Type.BABY);
        castle.packBricks(ColorType.RED);
        System.out.println(castle);
    }
}
