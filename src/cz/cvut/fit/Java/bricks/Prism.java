package cz.cvut.fit.Java.bricks;

import cz.cvut.fit.Java.bricks.Brick;
import cz.cvut.fit.Java.bricks.ColorType;
public class Prism extends Brick
{
    private final String CZ = "Hranol";
    public Prism(ColorType colorType, Type type) {
        super(colorType, type);
    }

    @Override
    public double getVolume() {
        double a = type.getSize();
        double baseH = Math.sqrt(a * a - (a / 2) * (a / 2));
        double baseVol = (a * baseH) / 2;
        return baseVol * a;
    }

    @Override
    public double getSurfaceArea() {
        double a = type.getSize();
        double baseH = Math.sqrt(a * a - (a / 2) * (a / 2));
        double baseVol = (a * baseH) / 2;
        return baseVol * 2 + a * a * 3;
    }
    @Override
    public String getCZ() {return CZ;}
}
