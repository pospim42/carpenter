package cz.cvut.fit.Java.bricks;

import cz.cvut.fit.Java.bricks.Brick;
import cz.cvut.fit.Java.bricks.ColorType;

public class Cube extends Brick
{
    private final String CZ = "Kostka";
    public Cube(ColorType colorType, Type type) {super(colorType, type);}
    @Override
    public double getVolume()
    {
        double a = type.getSize();
        return a*a*a;
    }
    @Override
    public double getSurfaceArea()
    {
        double a = type.getSize();
        return a*a*6;
    }
    @Override
    public String getCZ() {return CZ;}
}
