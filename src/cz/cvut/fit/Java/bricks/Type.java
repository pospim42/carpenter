package cz.cvut.fit.Java.bricks;

public enum Type
{
    BABY(6, "Nejmenší"), CHILD(3, "Dětská");
    private final double size;
    private final String CZ;

    Type(double size, String cz) {
        this.size = size; this.CZ = cz; }

    public double getSize() {
        return size;
    }

    public String getCZ() {
        return CZ;
    }
}
