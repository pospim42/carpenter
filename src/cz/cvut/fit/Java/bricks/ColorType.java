package cz.cvut.fit.Java.bricks;
public enum ColorType
{
    YELLOW("Žlutá"), RED("Červená"), GREEN("Zelená"), BLUE("Modrá"), WHITE("Bílá");
    private final String CZ;

    ColorType(String cz) {CZ = cz;}

    public String getCZ() {
        return CZ;
    }
}