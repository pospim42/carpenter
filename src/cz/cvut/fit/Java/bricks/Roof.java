package cz.cvut.fit.Java.bricks;

import cz.cvut.fit.Java.bricks.Brick;
import cz.cvut.fit.Java.bricks.ColorType;

public class Roof extends Brick
{
    private final String CZ = "Střecha";
    public Roof(ColorType colorType, Type type)
    {
        super(colorType, type);
    }
    @Override
    public double getVolume()
    {
        double a = type.getSize()/2;
        double baseVol = 2*a*a*a;
        double topVol = ((Math.PI*a*a)/2) * a;
        return baseVol + topVol;
    }
    @Override
    public double getSurfaceArea()
    {
        double a = type.getSize()/2;
        double baseArea = 2*a*a*5;
        double topArea = Math.PI * a * a + Math.PI*a*a;
        return baseArea + topArea;
    }
    @Override
    public String getCZ() {return CZ;}
}
