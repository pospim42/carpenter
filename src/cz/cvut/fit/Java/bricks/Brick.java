package cz.cvut.fit.Java.bricks;

import cz.cvut.fit.Java.bricks.Sets.Prices;

import java.util.Objects;

public abstract class Brick // základní společné vlastnosti,
        // potomek doplní abstract na konkrétní
{
    private String CZ;
    protected ColorType colorType;
    protected Type type;
    public Brick(ColorType colorType, Type type)
    {
        this.colorType = colorType;
        this.type = type;
    }

    public abstract double getVolume();
    public abstract double getSurfaceArea();
    public double getPaintPrice() {return getSurfaceArea()/10_000 * Prices.paintPrice;}
    public double getWoodPrice() {return getVolume()/1000_000 * Prices.woodPrice;}

    public ColorType getColorType() {
        return colorType;
    }
    public Type getType() {
        return type;
    }
    public String getShape() {
        return this.getClass().getSimpleName().toUpperCase();
    }
    @Override
    public String toString() {
        return String.format("%s %s %s", this.getShape(), getColorType(), getType());
    }
    public String getCZ() {return CZ;}
    public String toStringCZ() {
        return String.format("%s %s %s", this.getCZ(), getColorType().getCZ(), getType().getCZ());
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Brick brick)) return false;
        return getColorType() == brick.getColorType() && getType() == brick.getType();
    }
    @Override
    public int hashCode() {
        return Objects.hash(getColorType(), getType(), getShape());
    }
}

