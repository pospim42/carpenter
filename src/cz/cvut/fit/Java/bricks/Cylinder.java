package cz.cvut.fit.Java.bricks;

import cz.cvut.fit.Java.bricks.Brick;
import cz.cvut.fit.Java.bricks.ColorType;

public class Cylinder extends Brick
{
    private final String CZ = "Válec";
    public Cylinder(ColorType colorType, Type type)
    {
        super(colorType, type);
    }
    @Override
    public double getVolume()
    {
        double a = type.getSize()/2;
        double base = Math.PI*a*a;
        return base * a;
    }

    @Override
    public double getSurfaceArea()
    {
        double a = type.getSize()/2;
        double base = 2*Math.PI*a;
        return base*a;
    }
    @Override
    public String getCZ() {return CZ;}
}
