package cz.cvut.fit.Java.bricks.Sets;
import cz.cvut.fit.Java.bricks.*;

public class Car extends Building
{
    private final String CZ = "Auto";
    public Car(String name, Type type) {
        super(name, type);
    }
    @Override
    public void addBrick(Brick brick)
    {
        bricks.put(brick, bricks.getOrDefault(brick, 0) + 1);
    }
    @Override
    public void packBricks(ColorType color)
    {
        int cubeCnt = (type.equals(Type.CHILD)) ? 10 : 6;

        Cube cube = new Cube(color, type);
        bricks.put(cube, cubeCnt);
        Prism prism = new Prism(color, type);
        bricks.put(prism,2);
        Roof roof = new Roof(color, type);
        bricks.put(roof,4);
        Cylinder cylinder = new Cylinder(ColorType.BLUE, type);
        bricks.put(cylinder, 4);
    }
    public String getCZ() {return CZ;}
}
