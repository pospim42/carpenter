package cz.cvut.fit.Java.bricks.Sets;
import cz.cvut.fit.Java.bricks.*;

public class CompleteSet extends Building
{
    private final String CZ = "Kompletní sada";
    public CompleteSet (String name, Type type) {super(name, type);}
    @Override
    public void addBrick(Brick brick)
    {
        bricks.put(brick, bricks.getOrDefault(brick, 0) + 1);
    }
    @Override
    public void packBricks(ColorType color)
    {
        Castle castle = new Castle("tmp", type);
        castle.packBricks(ColorType.RED);
        Car car = new Car("tmp", type);
        car.packBricks(ColorType.GREEN);
        Cottage cottage = new Cottage("tmp", type);
        cottage.packBricks(ColorType.BLUE);
        bricks = castle.getBricks();
        bricks.putAll(car.getBricks());
        bricks.putAll(cottage.getBricks());
    }
    public String getCZ() {
        return CZ;
    }
}
