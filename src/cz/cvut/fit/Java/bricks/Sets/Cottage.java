package cz.cvut.fit.Java.bricks.Sets;

import cz.cvut.fit.Java.bricks.*;

public class Cottage extends Building
{
    private final String CZ = "Chata";
    public Cottage(String name, Type type) {
        super(name, type);
    }
    @Override
    public void addBrick(Brick brick)
    {
        bricks.put(brick, bricks.getOrDefault(brick, 0) + 1);
    }
    @Override
    public void packBricks(ColorType color)
    {
        int cubeCnt = (type.equals(Type.CHILD)) ? 19 : 12;
        int prismCnt = (type.equals(Type.CHILD)) ? 4 : 2;

        Cube cube = new Cube(color, type);
        bricks.put(cube, cubeCnt);
        Prism prism = new Prism(color, type);
        bricks.put(prism,prismCnt);
        Roof roof = new Roof(color, type);
        bricks.put(roof,2);
    }
    public String getCZ() {
        return CZ;
    }
}
