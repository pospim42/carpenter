package cz.cvut.fit.Java.bricks.Sets;
import cz.cvut.fit.Java.bricks.*;

public class EconomicalSet extends Building
{
    private final String CZ = "Ekonomická sada";
    public EconomicalSet (String name, Type type) {super(name, type);}
    @Override
    public void addBrick(Brick brick)
    {
        bricks.put(brick, bricks.getOrDefault(brick, 0) + 1);
    }
    @Override
    public void packBricks(ColorType color)
    {
        int cubeCnt = (type.equals(Type.CHILD)) ? 24 : 12;
        int cylinderCnt = (type.equals(Type.CHILD)) ? 12 : 8;
        int prismCnt = (type.equals(Type.CHILD)) ? 4 : 2;
        int roofCnt = (type.equals(Type.CHILD)) ? 8 : 4;

        Cube cube = new Cube(color, type);
        bricks.put(cube, cubeCnt);
        Prism prism = new Prism(color, type);
        bricks.put(prism,prismCnt);
        Roof roof = new Roof(color, type);
        bricks.put(roof,roofCnt);
        Cylinder cylinder = new Cylinder(ColorType.BLUE, type);
        bricks.put(cylinder, cylinderCnt);
    }
    public String getCZ() {
        return CZ;
    }
}