package cz.cvut.fit.Java.bricks.Sets;

import cz.cvut.fit.Java.bricks.*;
public class Castle extends Building
{
    private final String CZ = "Hrad";
    public Castle(String name, Type type) {
        super(name, type);
    }
    @Override
    public void addBrick(Brick brick)
    {
        bricks.put(brick, bricks.getOrDefault(brick, 0) + 1);
    }
    public void packBricks(ColorType color)
    {
        int cylinderCnt = (type.equals(Type.CHILD)) ? 12 : 8;
        int cubeCnt = (type.equals(Type.CHILD)) ? 23 : 11;
        int roofCnt = (type.equals(Type.CHILD)) ? 8 : 4;

        Cube cube = new Cube(color, type);
        bricks.put(cube, cubeCnt);
        Roof roof = new Roof(color, type);
        bricks.put(roof,roofCnt);
        Cylinder cylinder = new Cylinder(ColorType.BLUE, type);
        bricks.put(cylinder, cylinderCnt);
    }
    public String getCZ() {return CZ;}
}
