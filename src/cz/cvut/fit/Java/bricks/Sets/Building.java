package cz.cvut.fit.Java.bricks.Sets;

import cz.cvut.fit.Java.bricks.*;
import java.util.HashMap;
import java.util.Map;

public abstract class Building
{
    Map<Brick, Integer> bricks = new HashMap<>();
    private final String name;
    protected final Type type;
    public Building(String name, Type type) {this.name = name; this.type = type;}
    public abstract void addBrick(Brick brick);
    public abstract void packBricks(ColorType color);
    @Override
    public String toString()
    {                                   // entrySet = key-val pair
        String s = name + "\n";
        double paintPrice = 0, woodPrice = 0;
        for (Map.Entry<Brick, Integer> entry : bricks.entrySet())
        {
            s += entry.getKey() + ": " + entry.getValue() + "\n";
            paintPrice += entry.getKey().getPaintPrice()*entry.getValue();
            woodPrice += entry.getKey().getWoodPrice()*entry.getValue();
        }
        return String.format("%sPaint price: %.2f,-\nWood price: %.2f,-", s, paintPrice, woodPrice);
    }
    public void printCZ()
    {                                   // entrySet = key-val pair
        String s = name + "\n";
        double paintPrice = 0, woodPrice = 0;
        for (Map.Entry<Brick, Integer> entry : bricks.entrySet())
        {
            s += entry.getKey().toStringCZ() + ": " + entry.getValue() + "\n";
            paintPrice += entry.getKey().getPaintPrice()*entry.getValue();
            woodPrice += entry.getKey().getWoodPrice()*entry.getValue();
        }
        System.out.printf("%sCena barvy: %.2f,-\nCena dřeva: %.2f,-", s, paintPrice, woodPrice);
    }
    public Map<Brick, Integer> getBricks() {return bricks;}
}
