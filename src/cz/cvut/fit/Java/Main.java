package cz.cvut.fit.Java;
import cz.cvut.fit.Java.bricks.*;
import cz.cvut.fit.Java.bricks.Sets.*;
import java.util.*;

/*
 - static metody se nedědí
 -
*/
public class Main
{
    public static void main(String[] args)
    {
        Tester test = new Tester();
        Cube cube = new Cube(ColorType.BLUE, Type.CHILD);
        test.testSet();
        test.printBrickCZ(cube);
        test.printBrick(cube);
    }
}